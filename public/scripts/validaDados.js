const cpfElement = document.querySelector('#cpf')
const telElement = document.querySelector('#tel')

var temp = false // Temporizador

const calc_digitos_posicoes = (digitos, posicoes = 10, soma_digitos = 0) => {

  // Garante que o valor é uma string
  digitos = digitos.toString();

  // Faz a soma dos dígitos com a posição
  // Ex. para 10 posições:
  //   0    2    5    4    6    2    8    8   4
  // x10   x9   x8   x7   x6   x5   x4   x3  x2  x1
  //   0 + 18 + 40 + 28 + 36 + 10 + 32 + 24 + 8 = 196
  for (var i = 0; i < digitos.length; i++) {
    // Preenche a soma com o dígito vezes a posição
    soma_digitos = soma_digitos + (digitos[i] * posicoes);

    // Subtrai 1 da posição
    posicoes--;

  }

  // Captura o resto da divisão entre soma_digitos dividido por 11
  // Ex.: 196 % 11 = 9
  soma_digitos = soma_digitos % 11;

  // Verifica se soma_digitos é menor que 2
  if (soma_digitos < 2) {
    // soma_digitos agora será zero
    soma_digitos = 0;
  } else {
    // Se for maior que 2, o resultado é 11 menos soma_digitos
    // Ex.: 11 - 9 = 2
    // Nosso dígito procurado é 2
    soma_digitos = 11 - soma_digitos;
  }

  // Concatena mais um dígito aos primeiro nove dígitos
  // Ex.: 025462884 + 2 = 0254628842
  var cpf = digitos + soma_digitos;

  // Retorna
  return cpf;

} // calc_digitos_posicoes

const validaCPF = (valor) => {

  // Garante que o valor é uma string
  valor = valor.toString();

  // Remove caracteres inválidos do valor
  valor = valor.replace(/[^0-9]/g, '');


  // Captura os 9 primeiros dígitos do CPF
  // Ex.: 02546288423 = 025462884
  var digitos = valor.substr(0, 9);

  // Faz o cálculo dos 9 primeiros dígitos do CPF para obter o primeiro dígito
  var novo_cpf = calc_digitos_posicoes(digitos);

  // Faz o cálculo dos 10 dígitos do CPF para obter o último dígito
  var novo_cpf = calc_digitos_posicoes(novo_cpf, 11);

  // Verifica se o novo CPF gerado é idêntico ao CPF enviado
  if (novo_cpf === valor) {
    // CPF válido
    return true;
  } else {
    // CPF inválido
    return false;
  }

}

cpfElement.addEventListener('keypress', () => {

  let input = cpfElement;

  if (input.value.length == 3) {
    cpfElement.value += '.'
  }
  if (input.value.length == 7) {
    cpfElement.value += '.'
  }
  if (input.value.length == 11) {
    cpfElement.value += '-'
  }



  // Limpa o temporizador
  if (temp) {
    clearTimeout(temp)
  }

  // Cria um timeout de meio segundo 
  temp = setTimeout(() => {

    input.classList.remove('valido')
    input.classList.remove('invalido')

    let cpfValidando = input.value

    // Validando
    let valida = validaCPF(cpfValidando)
    console.log(cpfValidando);

    // Verificando se o CPF tá valido

    if (valida) {
      input.classList.add('valido')

    } else {
      input.classList.add('invalido')
    }



  }, 500);





})


const validaTelefone = (valor) =>{

  telElement = document.querySelector('#tel')
  telValue = telElement.value

   // Garante que o valor é uma string
   valor = valor.toString();
    
   // Remove caracteres inválidos do valor
   valor = valor.replace(/[^0-9]/g, '');

}

telElement.addEventListener('keypress', () => {

  let telElement = document.querySelector('#tel')
  let telValue = telElement.value
  if (telValue.length == 0) {
    telElement.value = '(' + telElement.value
  }
  if (telValue.length == 3) {
    telElement.value = telElement.value + ')' + " "
  }

  if (telValue.length == 10) {
    telElement.value = telElement.value + '-'
  }
})

const validaSenha = (senha1, senha2) => {
  if (senha1 != senha2) {
    return false
  }
  return true
}

const validaEmail = (email1, email2) => {
  if (email1 != email2) {
    return false
  }
  return true
}